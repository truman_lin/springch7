package truman.testbed.springapp;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class HelloController implements Controller {
	private String viewPage;

	public ModelAndView handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String user=req.getParameter("user");
		String message="A Spring MVC Test.";
		Map<String,String> params = new HashMap<String,String>();
		params.put("user",user);
		params.put("message", message);
		ModelAndView MnV=new ModelAndView(viewPage,params);
		return MnV;
	}
	public void setViewPage(String viewPage) {
		this.viewPage = viewPage;
	}
	

}
