package truman.testbed.springapp;

import java.beans.PropertyEditorSupport;

public class UserPropertyEditor extends PropertyEditorSupport {
    public String getAsText() {
        Object o = this.getValue();
        
        if(o == null || !(o instanceof User1)) {
            return null;
        }
        
        User1 user = (User1) o;       
        String name = user.getFirstName() 
                       + "," + user.getLastName();       
        return name;
    }
    
    public void setAsText(String text) {
        String[] tokens = text.split(",");
        
        User1 user = new User1();
        user.setFirstName(tokens[0]);
        user.setLastName(tokens[1]);
        
        setValue(user);
    }
}